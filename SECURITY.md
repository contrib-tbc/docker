# Security Policy

## Supported Versions

Security fixes and updates are only applied to the latest released version. So always try to be up to date.

## Reporting a Vulnerability

In order to minimize risks of attack while investigating and fixing the issue, any vulnerability shall be reported by 
opening a [**confidential** issue on gitlab.com](https://gitlab.com/to-be-continuous/docker/-/issues/new).

Follow-up and fixing will be made on a _best effort_ basis.

If you have doubts about a potential vulnerability, please reach out one of the maintainers on Discord.
