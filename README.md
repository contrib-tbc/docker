# GitLab CI template for Docker

This project implements a GitLab CI/CD template to build, check and inspect your containers with [Docker](https://www.docker.com/).

## Usage

In order to include this template in your project, add the following to your `.gitlab-ci.yml` :

```yaml
include:
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker.yml'
```

## Understanding the Docker template

The template supports two ways of building your Docker images:

1. The former **Docker-in-Docker** technique, that was widely used for years because of no other alternative, but that
  is now commonly recognized to have **significant security issues** ([read this post](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) for more info),
2. Or using [kaniko](https://github.com/GoogleContainerTools/kaniko), an open-source tool from Google for building Docker
  images, and that solves Docker-in-Docker security issues (and also speeds-up build times).

By default, the template uses the [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) way, but you may
activate the Docker-in-Docker build at your own risks by setting `DOCKER_DIND_BUILD` to `true` (see below).
:warning: In that case, make sure your runner has required privileges to run Docker-in-Docker ([see GitLab doc](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor)).

### Global variables

The Docker template uses some global configuration used throughout all jobs.

| Name                  | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `DOCKER_DIND_BUILD`   | Set to enable Docker-in-Docker build (:warning: unsecured, requires privileged runners). | _(none)_ (kaniko build by default) |
| `DOCKER_KANIKO_IMAGE` | The Docker image used to run kaniko - _for kaniko build only_ | `gcr.io/kaniko-project/executor:debug` (use `debug` images for GitLab) |
| `DOCKER_IMAGE`        | The Docker image used to run the docker client (see [full list](https://hub.docker.com/r/library/docker/)) - _for Docker-in-Docker build only_ | `registry.hub.docker.com/library/docker:latest`  |
| `DOCKER_DIND_IMAGE`   | The Docker image used to run the Docker daemon (see [full list](https://hub.docker.com/r/library/docker/)) - _for Docker-in-Docker build only_ | `registry.hub.docker.com/library/docker:dind`    |
| `DOCKER_FILE`         | The path to your `Dockerfile`          | `./Dockerfile`    |
| `DOCKER_CONTEXT_PATH` | The Docker [context path](https://docs.docker.com/engine/reference/commandline/build/#build-with-path) (working directory) | _none_ _only set if you want a context path different from the Dockerfile location_ |

In addition to this, the template supports _standard_ Linux proxy variables:

| Name                  | Description                                 | Default value |
| --------------------- | ------------------------------------------- | ------------- |
| `http_proxy`          | Proxy used for http requests                | _none_        |
| `https_proxy`         | Proxy used for https requests               | _none_        |
| `no_proxy`            | List of comma-separated hosts/host suffixes | _none_        |


### Images

For each Dockerfile, the template builds an image that may be [pushed](https://docs.docker.com/engine/reference/commandline/push/)
as two distinct images, depending on a certain _workflow_:

1. **snapshot**: the image is first built from the Dockerfile and then pushed to some Docker registry as
  the **snapshot** image. It can be seen as the raw result of the build, but still **untested and unreliable**.
2. **release**: once the snapshot image has been thoroughly tested (both by `package-test` stage jobs and/or `acceptance`
  stage jobs after being deployed to some server), then the image is pushed one more time as the **release** image.
  This second push can be seen as the **promotion** of the snapshot image being now **tested and reliable**.

In practice:

* the **snapshot** image is **always pushed** by the template (pipeline triggered by a Git tag or commit on any branch),
* the **release** image is only pushed:
    * on a pipeline triggered by a Git tag,
    * on a pipeline triggered by a Git commit on `master`.

The **snapshot** and **release** images are defined by the following variables:

| Name                      | Description           | Default value                                     |
| ------------------------- | --------------------- | ------------------------------------------------- |
| `DOCKER_SNAPSHOT_IMAGE`   | Docker snapshot image | `$CI_REGISTRY_IMAGE/snapshot:$CI_COMMIT_REF_SLUG` |
| `DOCKER_RELEASE_IMAGE`    | Docker release image  | `$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME`          |


As you can see, the Docker template is configured by default to use the GitLab container registry.
You may perfectly override this and use another Docker registry, but be aware of a few things:

* the `DOCKER_SNAPSHOT_IMAGE` requires a Docker registry that allows tag overwrite,
* the `DOCKER_RELEASE_IMAGE` _may_ use a Docker registry that doesn't allow tag overwrite, but:
    1. you should avoid overwriting a Git tag (at it will obviously fail while trying to (re)push the Docker image),
    2. you have to deactivate publish on `master` branch by setting the `$PUBLISH_ON_PROD` variable to `false` (as it would lead to the `master` tag being overwritten).

### Registries and credentials

As seen in the previous chapter, the Docker template uses by default the GitLab registry to push snapshot and release images.
Thus it makes use of credentials provided by GitLab itself to login (`CI_REGISTRY_USER` / `CI_REGISTRY_PASSWORD`).

But when using other registry(ies), you'll have also to **configure appropriate Docker credentials**.

#### Using the same registry for snapshot and release

If you use the **same registry** for both snapshot and release images, you shall use the following configuration

variables:

| Name                             | Description                            |
| -------------------------------- | -------------------------------------- |
| :lock: `DOCKER_REGISTRY_USER`    | Docker registry username for image registry |
| :lock: `DOCKER_REGISTRY_PASSWORD`| Docker registry password for image registry  |


#### Using different registries for snapshot and release

If you use **different registries** for snapshot and release images, you shall use separate configuration variables:

| Name                                     | Description                            |
| ---------------------------------------- | -------------------------------------- |
| :lock: `DOCKER_REGISTRY_SNAPSHOT_USER`   | Docker registry username for snapshot image registry |
| :lock: `DOCKER_REGISTRY_SNAPSHOT_PASSWORD`| Docker registry password for snapshot image registry |
| :lock: `DOCKER_REGISTRY_RELEASE_USER`    | Docker registry username for release image registry |
| :lock: `DOCKER_REGISTRY_RELEASE_PASSWORD`| Docker registry password for release image registry |



#### Setting your own Docker configuration file (advanced)

There might be cases where you need to provide the complete [Docker configuration file](https://docs.docker.com/engine/reference/commandline/cli/#configuration-files):

* need to declare authentication credentials for other registries than the 2 predefined ones (snapshot & release), 
* need to declare a [credentials store](https://docs.docker.com/engine/reference/commandline/login/#credentials-store) (ex: in order to [publish to Amazon ECR](https://github.com/GoogleContainerTools/kaniko#pushing-to-amazon-ecr) with Kaniko for instance),
* need to declare [proxies](https://docs.docker.com/engine/reference/commandline/cli/#automatic-proxy-configuration-for-containers),
* ...

If you are in one of those cases, you will need to use the `DOCKER_CONFIG_FILE` variable, expected to declare the path to your custom Docker configuration file (JSON). You may:

* leave the default value (`.docker/config.json`) or override it to some alternate location in your project repository and create the file **without any secret in it** using our dynamic variables replacement (see below),
* or override it as a GitLab project variable of type [File](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types), possibly inlining your secret credentials in it.

| Name                      | Description           | Default value                                     |
| ------------------------- | --------------------- | ------------------------------------------------- |
| `DOCKER_CONFIG_FILE`      | Path to the Docker configuration file (JSON) | `.docker/config.json` |

Moreover, this file supports **dynamic environment variables replacement**. 
That means it may contain references to other environment variables (in the format `${variable_name}`) that will be dynamically replaced
by the template before evaluation.
In addition to you own defined variables, you may use the following variables (provided and managed by the template):

* `${docker_snapshot_authent_token}`: the authentication token required by the snapshot registry (computed from configured `DOCKER_REGISTRY_SNAPSHOT_USER` / `DOCKER_REGISTRY_SNAPSHOT_PASSWORD` variables)
* `${docker_snapshot_registry_host}`: the snapshot registry host (based on the configured `DOCKER_SNAPSHOT_IMAGE ` variable)
* `${docker_release_authent_token}`: the authentication token required by the release registry (computed from configured `DOCKER_REGISTRY_RELEASE_USER` / `DOCKER_REGISTRY_RELEASE_PASSWORD` variables)
* `${docker_release_registry_host}`: the release registry host (based on the configured `DOCKER_RELEASE_IMAGE ` variable)

Example 1: Docker configuration file inlined in the project repository (`.docker/config.json`) with **dynamic variables replacement**:

```json
{
    "auths": {
        "${docker_snapshot_registry_host}": {
            "auth": "${docker_release_authent_token}"
        },
        "${docker_release_registry_host}": {
            "auth": "${docker_snapshot_authent_token}"
        },
        "my-readonly-repo-to-pull": {
            "auth": "${MY_OWN_REGISTRY_TOKEN}"
        }
    }
}
```

This file uses:

* template-managed `${docker_snapshot_authent_token}`, `${docker_snapshot_registry_host}`, `${docker_release_authent_token}` and `${docker_release_registry_host}` variables,
* the user-defined `${MY_OWN_REGISTRY_TOKEN}` (:information_source: an authentication token can be obtained with command `echo "user:password" | base64` and then be stored as a masked GitLab CI/CD project variable).

Example 2: Docker configuration file declared as a GitLab project variable of type [File](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types) with **dynamic variables replacement**:


```json
{
    "auths": {
        "$${docker_snapshot_registry_host}": {
            "auth": "$${docker_release_authent_token}"
        },
        "$${docker_release_registry_host}": {
            "auth": "$${docker_snapshot_authent_token}"
        },
        "my-readonly-repo-to-pull": {
            "auth": "ZG9ja2VyZHVkZTpnb3RjaGEh"
        }
    }
}
```

This file uses:

* template-managed `${docker_snapshot_authent_token}`, `${docker_snapshot_registry_host}`, `${docker_release_authent_token}` and `${docker_release_registry_host}` variables (:warning: mind the double `$$` to prevent GitLab from [trying to evaluate the variable](https://docs.gitlab.com/ee/ci/variables/index.html#use-the--character-in-variables)),
* the user-defined authentication may be inlined as a GitLab project variable is a place safe enough to store secrets.

## Multi Dockerfile support

This template supports building multiple Docker images from a single Git repository.

You can define the images to build using the [parallel matrix jobs](https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-jobs)
pattern inside the `.docker-base` job (this is the top parent job of all Docker template jobs).

Since each job in the template extends this base job, the pipeline will produce one job instance per image to build.
You can independently configure each instance of these jobs by redefining the variables described throughout this
documentation.

For example, if you want to build two Docker images, you must specify where the Dockerfiles are located and where the
resulting images will be stored.
You can do so by adding a patch to the `.docker-base` job in your `.gitlab-ci.yml` file so that it looks like this:

```yaml
.docker-base:
  parallel:
    matrix:
    - DOCKER_FILE: "front/Dockerfile"
      DOCKER_SNAPSHOT_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/front:$CI_COMMIT_REF_SLUG"
      DOCKER_RELEASE_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/front:$CI_COMMIT_REF_NAME"
    - DOCKER_FILE: "back/Dockerfile"
      DOCKER_SNAPSHOT_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/back:$CI_COMMIT_REF_SLUG"
      DOCKER_RELEASE_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/back:$CI_COMMIT_REF_NAME"
```

If you need to redefine a variable with the same value for all your Dockerfiles, you can just declare this variable as a global variable. For example, if you want to build all your images using Docker-in-Docker, you can simply define the `DOCKER_DIND_BUILD` variable as a global variable:

```yaml
variables:
  DOCKER_DIND_BUILD: "true"
```

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

## Jobs

### `docker-lint` job

This job performs a [Lint](https://github.com/projectatomic/dockerfile_lint) on your `Dockerfile`.

It is bound to the `build` stage, and uses the following variables:

| Name                  | Description                            | Default value                           |
| --------------------- | -------------------------------------- | --------------------------------------- |
| `DOCKER_LINT_IMAGE`   | The dockerlint image                   | `registry.hub.docker.com/projectatomic/dockerfile-lint:latest`  |
| `DOCKER_LINT_ARGS`    | Additional `dockerfile_lint` arguments | _(none)_                                |

In case you have to disable some rules, copy and edit the [rules](https://github.com/projectatomic/dockerfile_lint#extending-and-customizing-rule-files) into `mycustomdockerlint.yml` and set `DOCKER_LINT_ARGS: '-r mycustomdockerlint.yml'`

### `docker-hadolint` job

This job performs a [Lint](https://github.com/hadolint/hadolint) on your `Dockerfile`.

It is bound to the `build` stage, and uses the following variables:

| Name                       | Description                            | Default value                           |
| -------------------------- | -------------------------------------- | --------------------------------------- |
| `DOCKER_HADOLINT_IMAGE`    | The Hadolint image                     | `registry.hub.docker.com/hadolint/hadolint:latest-alpine`       |
| `DOCKER_HADOLINT_ARGS`     | Additional `hadolint` arguments        | _(none)_                        |

In case you have to disable some rules, either add `--ignore XXXX` to the `DOCKER_HADOLINT_ARGS` variable or create a [Hadolint configuration file](https://github.com/hadolint/hadolint#configure) named `hadolint.yaml` at the root of your repository.

You can also use [inline ignores](https://github.com/hadolint/hadolint#inline-ignores) in your Dockerfile:

```Dockerfile
# hadolint ignore=DL3006
FROM ubuntu

# hadolint ignore=DL3003,SC1035
RUN cd /tmp && echo "hello!"
```

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/docker-hadolint-*.native.json`      | native hadolint test report (json) | [DefectDojo integration](https://defectdojo.github.io/django-DefectDojo/integrations/parsers/#hadolint)<br/>_This report is generated only if DefectDojo template is detected_ |
| `reports/docker-hadolint-*.codeclimate.json` | hadolint (GitLab) codeclimate format | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscodequality) |

### `docker-build` job

This job builds the image and publishes it to the _snapshot_ repository.

It is bound to the `package-build` stage, and uses the following variables:

| Name                            | Description                                                                                                   | Default value                  |
| ------------------------------- | ------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| `DOCKER_BUILD_ARGS`             | Additional `docker build`/`kaniko` arguments                                                                  | _(none)_                       |
| `DOCKER_REGISTRY_MIRROR`        | URL of a Docker registry mirror to use during the image build (instead of default `https://index.docker.io`)  | _(none)_                       |
| `DOCKER_METADATA`               | Additional `docker build`/`kaniko` arguments to set label                                                     | OCI Image Format Specification |

This job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

| Name                  | Description                                            | Example                                 |
| --------------------- | ------------------------------------------------------ | --------------------------------------- |
| `docker_image`        | snapshot image name **with tag**                       | `registry.gitlab.com/acme/website/snapshot:main` |
| `docker_image_digest` | snapshot image name **with digest** (no tag)           | `registry.gitlab.com/acme/website/snapshot@sha256:b7914a91...` |
| `docker_repository`   | snapshot image **bare repository** (no tag nor digest) | `registry.gitlab.com/acme/website/snapshot`      |
| `docker_tag`          | snapshot image tag                                     | `main`                                  |
| `docker_digest`       | snapshot image digest                                  | `sha256:b7914a91...`                    |

They may be freely used in downstream jobs (for instance to deploy the upstream built Docker image, whatever the branch or tag).

If you want to use GitLab CI variables or any other variable in your Dockerfile, you can add them to `DOCKER_BUILD_ARGS` like so:

```yaml
DOCKER_BUILD_ARGS: "--build-arg CI_PROJECT_URL --build-arg MY_VAR='MY_VALUE'"
```

These variables will then be available for use in your Dockerfile:

```Dockerfile
FROM scratch

ARG CI_PROJECT_URL
ARG MY_VAR
LABEL name="my-project"                   \
      description="My Project: $MY_VAR"   \
      url=$CI_PROJECT_URL                 \
      maintainer="my-project@acme.com"
```

Default value for `DOCKER_METADATA` supports a subset of the [OCI Image Format Specification](https://github.com/opencontainers/image-spec/blob/master/annotations.md) for labels and use [GitLab CI pre-defined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to guess the value as follow :

| Label                               | Gitlab CI pre-defined variable |
| ----------------------------------- | ------------------------------ |
| `org.opencontainers.image.url`      | `$CI_PROJECT_URL`              |
| `org.opencontainers.image.source`   | `$CI_PROJECT_URL`              |
| `org.opencontainers.image.title`    | `$CI_PROJECT_PATH`             |
| `org.opencontainers.image.ref.name` | `$CI_COMMIT_REF_NAME`          |
| `org.opencontainers.image.revision` | `$CI_COMMIT_SHA`               |
| `org.opencontainers.image.created`  | `$CI_JOB_STARTED_AT`           |

Note that spaces are currently not supported by Kaniko. Therefore, title couldn't be `CI_PROJECT_TITLE`.

You may disable this feature by setting `DOCKER_METADATA` to empty or you can override some of the pre-defined label value with the `DOCKER_BUILD_ARGS`.

```yaml
DOCKER_BUILD_ARGS: "--label org.opencontainers.image.title=my-project"
```

If you have defined one of those labels in the Dockerfile, the final value will depend if image is built with Kaniko or Docker in Docker. With Kaniko, the value of the Dockerfile take precedence, while with DinD command-line argument take precedence.

### `docker-healthcheck` job

:warning: this job requires that your runner has required privileges to run [Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor).
If it is not the case this job will not be run.

This job performs a [Health Check](https://docs.docker.com/engine/reference/builder/#healthcheck) on your built image.

It is bound to the `package-test` stage, and uses the following variables:

| Name                                   | Description                                                          | Default value     |
| -------------------------------------- | -------------------------------------------------------------------- | ----------------- |
| `DOCKER_HEALTHCHECK_DISABLED`          | Set to `true` to disable health check                                          | _(none: enabled by default)_ |
| `DOCKER_HEALTHCHECK_TIMEOUT`           | When testing a Docker Health (test stage), how long (in seconds) wait for the [HealthCheck status](https://docs.docker.com/engine/reference/builder/#healthcheck) | `60` |
| `DOCKER_HEALTHCHECK_OPTIONS`           | Docker options for health check such as port mapping, environment... | _(none)_ |
| `DOCKER_HEALTHCHECK_CONTAINER_ARGS`    | Set arguments sent to the running container for health check         | _(none)_ |

In case your Docker image is not intended to run as a service and only contains a *client tool* (like curl, Ansible, ...) you can test it by overriding the Health Check Job. See [this example](#overriding-docker-healthcheck).

:warning: Keep in mind that the downloading of the snapshot image by the GitLab runner will be done during the waiting time (max `DOCKER_HEALTHCHECK_TIMEOUT`).
In case your image takes quite some time to be downloaded by the runner, increase the value of `DOCKER_HEALTHCHECK_TIMEOUT` in your `.gitlab-ci.yml` file.

### `docker-trivy` job

This job performs a Vulnerability Static Analysis with [Trivy](https://github.com/aquasecurity/trivy) on your built image.

Without any configuration Trivy will run in [standalone](https://aquasecurity.github.io/trivy/v0.28.0/docs/references/modes/standalone/) mode.

If you want to run Trivy in client/server mode, you need to set the `DOCKER_TRIVY_ADDR` environment variable.

```yaml
variables:
  DOCKER_TRIVY_ADDR: "https://trivy.acme.host"
```

It is bound to the `package-test` stage, and uses the following variables:

| Name                   | Description                            | Default value     |
| ---------------------- | -------------------------------------- | ----------------- |
| `DOCKER_TRIVY_IMAGE`   | The docker image used to scan images with Trivy | `registry.hub.docker.com/aquasec/trivy:latest` |
| `DOCKER_TRIVY_ADDR`    | The Trivy server address (for client/server mode)              | _(none: standalone mode)_  |
| `DOCKER_TRIVY_SECURITY_LEVEL_THRESHOLD`| Severities of vulnerabilities to be displayed (comma separated values: `UNKNOWN`, `LOW`, `MEDIUM`, `HIGH`, `CRITICAL`) | `UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL`  |
| `DOCKER_TRIVY_DISABLED`| Set to `true` to disable Trivy analysis          | _(none)_ |
| `DOCKER_TRIVY_ARGS`    | Additional [`trivy client` arguments](https://aquasecurity.github.io/trivy/v0.27.1/docs/references/cli/client/)  | `--ignore-unfixed --vuln-type os` |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/docker-trivy-*.native.json` | native Trivy report format (json) | [DefectDojo integration](https://defectdojo.github.io/django-DefectDojo/integrations/parsers/#trivy)<br/>_This report is generated only if DefectDojo template is detected_ |
| `reports/docker-trivy-*.gitlab.json` | [Trivy report format for GitLab](https://aquasecurity.github.io/trivy/v0.30.4/docs/integrations/gitlab-ci/) format | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscontainer_scanning) |

### `docker-sbom` job

This job generates a [SBOM](https://cyclonedx.org/) file listing installed packages using [syft](https://github.com/anchore/syft).

It is bound to the `package-test` stage, and uses the following variables:

| Name                  | description                            | default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `DOCKER_SBOM_DISABLED` | Set to `true` to disable this job | _none_ |
| `DOCKER_SBOM_IMAGE` | The docker image used to emit SBOM | `registry.hub.docker.com/anchore/syft:debug` |
| `DOCKER_SBOM_OPTS` | Options for syft used for SBOM analysis | `--catalogers rpm-db-cataloger,alpmdb-cataloger,apkdb-cataloger,dpkgdb-cataloger,portage-cataloger` |

### `docker-publish` job

This job pushes (_promotes_) the built image as the _release_ image [skopeo](https://github.com/containers/skopeo).

| Name                  | Description                                                                 | Default value     |
| --------------------- | --------------------------------------------------------------------------- | ----------------- |
| `DOCKER_SKOPEO_IMAGE` | The Docker image used to run [skopeo](https://github.com/containers/skopeo) | `quay.io/skopeo/stable:latest` |
| `DOCKER_PUBLISH_ARGS` | Additional [`skopeo copy` arguments](https://github.com/containers/skopeo/blob/master/docs/skopeo-copy.1.md#options) | _(none)_          |
| `AUTODEPLOY_TO_PROD`  | Set to enable automatic publish (and deploy) on `master` branch             | _none_ (enabled)  |
| `PUBLISH_ON_PROD`     | Determines whether this job is enabled on `master` branch                   | `true`_ (enabled)  |
| `DOCKER_SEMREL_RELEASE_DISABLED` | Set to `true` to disable [semantic-release integration](#semantic-release-integration)   | _none_ (enabled) |

This job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

| Name                  | Description                                           | Example                                 |
| --------------------- | ----------------------------------------------------- | --------------------------------------- |
| `docker_image`        | release image name **with tag**                       | `registry.gitlab.com/acme/website:main` |
| `docker_image_digest` | release image name **with digest** (no tag)           | `registry.gitlab.com/acme/website@sha256:b7914a91...` |
| `docker_repository`   | release image **bare repository** (no tag nor digest) | `registry.gitlab.com/acme/website`      |
| `docker_tag`          | release image tag                                     | `main`                                  |
| `docker_digest`       | release image digest                                  | `sha256:b7914a91...`                    |

They may be freely used in downstream jobs (for instance to deploy the upstream built Docker image, whatever the branch or tag).

### `semantic-release` integration

If you activate the [`semantic-release-info` job from the `semantic-release` template](https://gitlab.com/to-be-continuous/semantic-release/#semantic-release-info-job), the `docker-publish` job will rely on the generated next version info.

* the release will only be performed if a semantic release is present
* the tag will be based on `SEMREL_INFO_NEXT_VERSION`, it will override `DOCKER_RELEASE_IMAGE` by simply substituting the tag, or adding a tag when there's none.

For instance, in both cases:

```yml
DOCKER_RELEASE_IMAGE: "registry.gitlab.com/$CI_PROJECT_NAME"
DOCKER_RELEASE_IMAGE: "registry.gitlab.com/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME"
```
The published Docker image will be `registry.gitlab.com/$CI_PROJECT_NAME:$SEMREL_INFO_NEXT_VERSION` and all subsequent jobs relying on the `docker_image` variable will be provided with this tag.

:warning: When `semantic-release` detects no release (i.e. either the semantic-release template is misconfigured, or there were simply no `feat`/`fix` commits), the `docker-publish` job will report a warning and *no image will be pushed* in the release registry. In such a case, the `docker_image` remains unchanged, and will refer to the snapshot image. Any subsequent job that may deploy to production (Kubernetes or Openshift), should thus be configured *not to deploy* in this situation. Refer to deployment template for more information.

Finally, the semantic-release integration can be disabled with the `DOCKER_SEMREL_RELEASE_DISABLED` variable.

## Examples

### Using the GitLab Docker registry

This sample is the easiest one as you just have nothing to do.

All template variables are configured by default to build and push your Docker images on the GitLab registry.

### Using an external Docker registry

With this template, you may perfectly use an external Docker registry (ex: a [JFrog Artifactory](https://www.jfrog.com/confluence/display/JFROG/Docker+Registry), a private Kubernetes registry, ...).

Here is a `.gitlab-ci.yaml` using an external Docker registry:

```yaml
include:
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker.yml'

variables:
  DOCKER_SNAPSHOT_IMAGE: "registry.acme.host/$CI_PROJECT_NAME/snapshot:$CI_COMMIT_REF_SLUG"
  DOCKER_RELEASE_IMAGE: "registry.acme.host/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME"
  # $DOCKER_REGISTRY_USER and $DOCKER_REGISTRY_PASSWORD are defined as secret GitLab variables
```

Depending on the Docker registry you're using, you may have to use a real password or generate a token as authentication credential.

### Building multiple Docker images

Here is a `.gitlab-ci.yaml` that builds 2 Docker images from the same project (uses [parallel matrix jobs](https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-jobs)):

```yaml
include:
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker.yml'

variables:
  DOCKER_DIND_BUILD: "true"

.docker-base:
  parallel:
    matrix:
    - DOCKER_FILE: "front/Dockerfile"
      DOCKER_SNAPSHOT_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/front/snapshot:$CI_COMMIT_REF_SLUG"
      DOCKER_RELEASE_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/front:$CI_COMMIT_REF_NAME"
    - DOCKER_FILE: "back/Dockerfile"
      DOCKER_SNAPSHOT_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/back/snapshot:$CI_COMMIT_REF_SLUG"
      DOCKER_RELEASE_IMAGE: "$CI_REGISTRY/$CI_PROJECT_PATH/back:$CI_COMMIT_REF_NAME"
```

## Variants

The Docker template can be used in conjunction with template variants to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Name              | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master` |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Name                             | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker.yml'
  # Vault variant
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker-vault.yml'

variables:
    # Secrets managed by Vault
    DOCKER_REGISTRY_SNAPSHOT_USER: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/artifactory/snapshot/credentials?field=user"
    DOCKER_REGISTRY_SNAPSHOT_PASSWORD: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/artifactory/snapshot/credentials?field=token"
    DOCKER_REGISTRY_RELEASE_USER: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/artifactory/release/credentials?field=user"
    DOCKER_REGISTRY_RELEASE_PASSWORD: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/artifactory/release/credentials?field=token"
    VAULT_BASE_URL: "https://vault.acme.host/v1"
    # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
